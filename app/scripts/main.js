function openNav() {
    document.getElementById("box-menu-mobile").style.width = "268px";
}

function closeNav() {
    document.getElementById("box-menu-mobile").style.width = "0";
}

$('#galeria-produtos').slick({
	infinite: true,
	speed: 300,
	slidesToShow: 3,
	slidesToScroll: 3,
	autoplay: true,
	autoplaySpeed: 5000,
	speed: 600,
	appendArrows: $(setas),
	responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,
				dots: true
			}
		},
		{
			breakpoint: 375,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}
	]
});

